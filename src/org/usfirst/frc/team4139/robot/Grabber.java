package org.usfirst.frc.team4139.robot;
import edu.wpi.first.wpilibj.Solenoid;

public class Grabber 
{

	public Solenoid grab1;
	
	public Grabber(int port)
	{
		grab1 = new Solenoid(port);
	}

	public void OpenClose(boolean b) 
	{	
        grab1.set(b);
	}
}