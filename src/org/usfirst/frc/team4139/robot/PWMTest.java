package org.usfirst.frc.team4139.robot;

import org.usfirst.frc.team4139.robot.TurnDir;
import edu.wpi.first.wpilibj.*;

//import org.usfirst.frc.team4139.robot.CAN.Timer;
//import com.ctre.CANTalon;
import edu.wpi.first.wpilibj.Timer;

import edu.wpi.first.wpilibj.Talon;

public class PWMTest

{
	
	private Timer timer;

    //initialize the motors

    private Talon l1;

    private Talon l2;

    private Talon l3;

    private Talon r1;

    private Talon r2;

    private Talon r3;

    //initialize the robot

    private RobotDrive robot;
    
    private RobotDrive robot1;

    //initialize the drive mode

    private int driveMode;

    public static final int TANK_DRIVE = 1;

    public static final int ARCADE_DRIVE = 2;

    

    public PWMTest(int le1,int le2,int le3,int ri1,int ri2,int ri3)

    {

        

        l1 = new Talon(le1);

        l2 = new Talon(le2);

        l3 = new Talon(le3);

        r1 = new Talon(ri1);

        r2 = new Talon(ri2);

        r3 = new Talon(ri3);

        l1.setSafetyEnabled(false);

        l2.setSafetyEnabled(false);

        l3.setSafetyEnabled(false);

        r1.setSafetyEnabled(false);

        r2.setSafetyEnabled(false);

        r3.setSafetyEnabled(false);

        

        driveMode = TANK_DRIVE;

        

        timer = new Timer();

        timer.reset();

        l1.setBounds(100,100,50,0,0);

        l2.setBounds(100,100,50,0,0);

        l3.setBounds(100,100,50,0,0);

        r1.setBounds(100,100,50,0,0);

        r2.setBounds(100,100,50,0,0);

        r3.setBounds(100,100,50,0,0);

        robot = new RobotDrive(l1,l2,r1,r2);
        
        robot1 = new RobotDrive(l3,r3);

        driveMode = TANK_DRIVE;

    }

    

    public void timedrive(double time,double lS,double rS)

    {

        timer.start();

        if(timer.get()<time)        

        {

            robot.drive(lS,rS);

        }

        timer.reset();

    }

    public void drive(double lS,double rS)

    {

        switch(driveMode)

        {

            case TANK_DRIVE:

                robot.tankDrive(-lS, -rS);

                break;

            case ARCADE_DRIVE:

                robot.arcadeDrive(-lS, -rS);

                break;

            default:

                robot.tankDrive(-lS, -rS);

        }

    }

    //switches to TankDrive

    public void switchToTank()

    {

        driveMode = TANK_DRIVE;

    }

        

    //switches to ArcadeDrive

    public void switchToArcade()

    {

        driveMode = ARCADE_DRIVE;

    }

}